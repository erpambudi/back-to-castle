extends Node

# declare our variables
var currentLevel
var lives = 0

# initialize stuff here
func _ready():
	
	# first determine if a Saves directory exists.
	# if it doesn't, create it.
	var dir = Directory.new()
	if !dir.dir_exists("res://SaveData"):
		dir.open("res://")
		dir.make_dir("res://SaveData")

# the following functions are getters and setters for the variables

# get the current lives
func get_lives():
	return lives

# set the lives
func set_lives(var amount):
	lives = amount

# get the current level name
func get_level():
	return get_tree().get_current_scene().get_filename()

# set the level
func set_level(var levelName):
	# currentLevel must be changed to the new level
	currentLevel = levelName
	get_tree().change_scene(currentLevel)

# the following functions save and load a game, depending on what the player does at the main/pause menus.
# at the end of each level, the save function is automatically called.

# first create a dictionary to store the save info in. Similar to a serializable class in Unity in which 
# the player data would be stored.
var GameData = {
	level="",
	lives=0
}

# this saves the current game state
func save_game_state(var saveName):
	
	# create a file object
	var saveGame = File.new()
	saveGame.open("res://SaveData/"+saveName+".sve", File.WRITE)
	
	# create a data object to store the current save data in
	var data = GameData
	
	# store the data
	data.level = get_level()
	data.lives = get_lives()
	
	# write the data to disk
	saveGame.store_line(to_json(data))
	saveGame.close()

# this loads a previously saved game state
func load_game_state(var saveName):
	
	# create a file object
	var loadGame = File.new()
	
	# see if the file actually exists before opening it
	if !loadGame.file_exists("res://SaveData/"+saveName+".sve"):
		print ("File not found! Aborting...")
		return
	
	# use an empty dictionary to assign temporary data to
	var currentLine = {}
	var DADOS = ""
		# read the data in
	loadGame.open("res://SaveData/"+saveName+".sve", File.READ)
	while(!loadGame.eof_reached()):
		DADOS = DADOS + loadGame.get_line()
		# use currentLine to parse through the file
	
	currentLine = parse_json(DADOS)
	
	# assign the data to the variables
	currentLevel  =  currentLine["level"]
	lives = currentLine["lives"]
	
	set_lives(lives)
	set_level(currentLevel)
	loadGame.close()


# this creates a new game altogether
func new_game(var nameScene):
	# intitalize default variables
	currentLevel = nameScene
	set_lives(3)
	set_level(currentLevel)
