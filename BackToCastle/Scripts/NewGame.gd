extends LinkButton


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(String) var scene_to_load
var scene_name = "res://Scenes/Level 1.tscn"
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_New_Game_pressed():
	Global.new_game(scene_name)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
